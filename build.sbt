name := "copris"
version := "2.3.2"

scalaVersion := "2.12.12"

scalacOptions ++= Seq("-deprecation", "-feature", "-unchecked", "-Xlint")

assemblyExcludedJars in assembly := {
  val cp = (fullClasspath in assembly).value
  cp filterNot { f =>
    f.data.getName.startsWith("sugar-") ||
    f.data.getName == "org.sat4j.core.jar" ||
    f.data.getName == "sat4j-pb.jar"
  }
}
assemblyJarName := s"${name.value}-all_${scalaBinaryVersion.value}-${version.value}.jar"
